package com.softhill.asyncemulation;

import com.softhill.asyncemulation.service.PriceFinderService;
import com.softhill.asyncemulation.service.SimpleExchangeService;

import java.util.List;
import java.util.function.Supplier;

public class Main {
    private static PriceFinderService bestPriceFinder = new PriceFinderService(new SimpleExchangeService());

    public static void main(String[] args) {
        execute("sequential", () -> bestPriceFinder.findPricesSequential("myPhone27S"));
        execute("parallel", () -> bestPriceFinder.findPricesParallel("myPhone27S"));
        execute("composed CompletableFuture", () -> bestPriceFinder.findPricesFuture("myPhone27S"));
        execute("combined USD CompletableFuture", () -> bestPriceFinder.findPricesInUSD("myPhone27S"));
    }

    /**
     * Show time of method execution
     * @param msg
     * @param s
     */
    private static void execute(String msg, Supplier<List<String>> s) {
        long fastest = 0;
        for (int i = 0; i < 10; i++) {
            long start = System.nanoTime();
            System.out.println(s.get());
            long duration = (System.nanoTime() - start) / 1_000_000;
            if(i == 0 || duration < fastest) {
                fastest = duration;
            }
        }
        System.out.println("----------" + msg + " done in " + fastest + " msecs");
    }
}
