package com.softhill.asyncemulation.enums;

public enum Money {
    USD(1.0), EUR(1.35387);

    private final double rate;

    Money(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }
}