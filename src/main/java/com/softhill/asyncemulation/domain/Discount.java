package com.softhill.asyncemulation.domain;

import static com.softhill.asyncemulation.util.AsyncUtil.delay;
import static com.softhill.asyncemulation.util.AsyncUtil.format;

/**
 * Class represents discount of quote of some shop
 */
public class Discount {

    public enum Code {
        NONE(0), SILVER(5), GOLD(10), PLATINUM(15), DIAMOND(20);

        private final int percentage;

        Code(int percentage) {
            this.percentage = percentage;
        }
    }

    /**
     * Calculate price using quote and combine it with shop name
     * @param quote
     * @return price applicable with discount with shop name
     */
    public static String applyDiscount(Quote quote) {
        return quote.getShopName() + " price is " + Discount.apply(quote.getPrice(), quote.getDiscountCode());
    }

    private static double apply(double price, Code code) {
        delay();
        return format(price * (100 - code.percentage) / 100);
    }
}
