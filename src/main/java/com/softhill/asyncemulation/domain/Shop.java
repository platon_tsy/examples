package com.softhill.asyncemulation.domain;

import java.util.Random;

import static com.softhill.asyncemulation.util.AsyncUtil.delay;
import static com.softhill.asyncemulation.util.AsyncUtil.format;

/**
 * Class represents shop
 */
public class Shop {

    private final String name;
    private final Random random;

    public Shop(String name) {
        this.name = name;
        random = new Random(name.charAt(0) * name.charAt(1) * name.charAt(2));
    }

    /**
     * Builds price message from random discount code
     * shop name and price delimited by ':'
     * @param product
     * @return price message
     */
    public String getPriceMessage(String product) {
        double price = calculatePrice(product);
        Discount.Code code = Discount.Code.values()[random.nextInt(Discount.Code.values().length)];
        return name + ":" + price + ":" + code;
    }


    public double getPrice(String product) {
        return calculatePrice(product);
    }

    /**
     * calculate price with delay
     * emulate long running task or remote service
     * @param product
     * @return price
     */
    public double calculatePrice(String product) {
        delay();
        return format(random.nextDouble() * product.charAt(0) + product.charAt(1));
    }

    public String getName() {
        return name;
    }
}
