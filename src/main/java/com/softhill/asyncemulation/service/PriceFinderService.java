package com.softhill.asyncemulation.service;

import com.softhill.asyncemulation.domain.Discount;
import com.softhill.asyncemulation.domain.Quote;
import com.softhill.asyncemulation.domain.Shop;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.softhill.asyncemulation.enums.Money.EUR;
import static com.softhill.asyncemulation.enums.Money.USD;

/**
 * Service for finding prices from remote shops
 */
public class PriceFinderService {

    private final IExchangeService exchangeService;
    private final List<Shop> shops = Arrays.asList(new Shop("BestPrice"),
                                                   new Shop("LetsSaveBig"),
                                                   new Shop("MyFavoriteShop"),
                                                   new Shop("BuyItAll"),
                                                   new Shop("ShopEasy"));

    private final Executor executor = Executors.newFixedThreadPool(shops.size(), r -> {
        Thread t = new Thread(r);
        t.setDaemon(true);
        return t;
    });

    public PriceFinderService(IExchangeService exchangeService) {
        this.exchangeService = exchangeService;
    }

    public List<String> findPricesSequential(String product) {
        return shops.stream()
                .map(shop -> shop.getPriceMessage(product))
                .map(Quote::parse)
                .map(Discount::applyDiscount)
                .collect(Collectors.toList());
    }

    public List<String> findPricesParallel(String product) {
        return shops.parallelStream()
                .map(shop -> shop.getPriceMessage(product))
                .map(Quote::parse)
                .map(Discount::applyDiscount)
                .collect(Collectors.toList());
    }

    public List<String> findPricesFuture(String product) {
        List<CompletableFuture<String>> priceFutures = findPricesStream(product)
                .collect(Collectors.toList());

        return priceFutures.stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
    }

    public Stream<CompletableFuture<String>> findPricesStream(String product) {
        return shops.stream()
                .map(shop -> CompletableFuture.supplyAsync(() -> shop.getPriceMessage(product), executor))
                .map(future -> future
                        .thenApply(Quote::parse))
                .map(future -> future
                        .thenCompose(quote -> CompletableFuture.supplyAsync(() -> Discount
                                .applyDiscount(quote), executor)));
    }

    public List<String> findPricesInUSD(String product) {
        Stream<CompletableFuture<String>> priceFuturesStream = shops
                .stream()
                .map(shop -> CompletableFuture
                        .supplyAsync(() -> shop.getPrice(product))
                        .thenCombine(
                                CompletableFuture.supplyAsync(() -> exchangeService.getRate(EUR, USD)),
                                (price, rate) -> price * rate)
                        .thenApply(price -> shop.getName() + " price is " + price));
        List<CompletableFuture<String>> priceFutures = priceFuturesStream.collect(Collectors.toList());
        List<String> prices = priceFutures
                .stream()
                .map(CompletableFuture::join)
                .collect(Collectors.toList());
        return prices;
    }

}
