package com.softhill.asyncemulation.service;

import com.softhill.asyncemulation.enums.Money;

public interface IExchangeService {
    double getRate(Money source, Money destination);
}
