package com.softhill.asyncemulation.service;


import com.softhill.asyncemulation.enums.Money;

import static com.softhill.asyncemulation.util.AsyncUtil.delay;

/**
 * Service for getting exchanging rate
 */
public class SimpleExchangeService implements IExchangeService {

    public double getRate(Money source, Money destination) {
        return getRateWithDelay(source, destination);
    }

    private static double getRateWithDelay(Money source, Money destination) {
        delay();
        return destination.getRate() / source.getRate();
    }

}
